/*
 * IBT-2 Speed Controller ABH.c
 *
 * Created: 16/04/2017 7:17:58 PM
 * Author : Ned
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "HeaderFiles/includes.h"

#define RAMP_UP		2
#define RAMP_DOWN	1
#define DEAD_BAND	10
#define MIN_SPEED	25
#define MAX_SPEED	255

typedef enum MotorState_t {INIT, TUNE, F_TUNE, R_TUNE, IDLE_WAIT, IDLE, F_DELAY, R_DELAY, FORWARD, REVERSE} MotorState_t;

uint8_t TimeYet(uint16_t compTime);

uint8_t tempByte;
char tempString[64];

adc_t speedPot;
uint16_t tuneMax, tuneMin;
uint8_t dataChanged;

uint16_t reqSpeed, curSpeed;

struct buttonsModule endstops;
MotorState_t motorState;

volatile uint16_t isrTime;
uint16_t currentTime;
uint16_t updateADC;
uint16_t updateMemory;
uint16_t sendSerial;
uint16_t flashLED;
uint16_t disableMotors;
uint16_t directionDelay;
uint16_t rampSpeed;
uint16_t idleEnter;

void SetupPorts(void) {
	// Fake ground/vcc
	DDRD = 0x10;
	DDRC = 0x05;
	PORTD = 0x00;
	PORTC = 0x04;
	
	POT_PUP = ENABLED;
	
	LED_DDR = ENABLED;
	
	LIM_F_DDR = DISABLED;
	LIM_F_PUP = ENABLED;
	LIM_R_DDR = DISABLED;
	LIM_R_PUP = ENABLED;
	
	TX_DDR = ENABLED;
	RX_DDR = DISABLED;
	RX_PUP = ENABLED;
	
	R_EN_DDR = ENABLED;
	F_EN_DDR = ENABLED;
	R_PWM_DDR = ENABLED;
	F_PWM_DDR = ENABLED;
	
	LED = OFF;
}

void SetupTimers(void) {
	TCCR1A = 0xA1;	// CoCM A and B, Phase Correct PWM 8 bit
	TCCR1B = 0x03;	// prescaler 64. 16e6/256/64 = 976.5625hz
		
	TCCR2A = 0x02;	// CTC
	TCCR2B = 0x04;	// 16e6/64/250 = 1KHz
	OCR2A = 249;	// 250 overflow
	TIMSK2 = 0x02;	// OCR2A interrupt
}

// release handler for end stops
void Release_Handler(struct buttonsModule *const module, uint32_t buttons) {
	if (buttons & (1 << LIMIT_FORWARD)) {
		uart_puts("Released Forward\r\n");
		
		if (tuneMax > nvMem.potMax) {
			dataChanged = 1;
			nvMem.potMax = tuneMax;
		}
		
		tuneMax = 0;
	}
	
	if (buttons & (1 << LIMIT_REVERSE)) {
		uart_puts("Released Reverse\r\n");
		
		if (tuneMin < nvMem.potMin) {
			dataChanged = 1;
			nvMem.potMin = tuneMin;
		}
		
		tuneMin = 0xFFFF;
	}
	
	nvMem.potMid = nvMem.potMax - nvMem.potMin;
	nvMem.potMid >>= 1;
	nvMem.potMid += nvMem.potMin;
}

// Init the buttons
void SetupButtons(void) {
	// Create a config struct
	struct buttonsConfig config;
	
	ButtonsGetConfigDefaults(&config);													// Set config struct to defaults
	config.rampEnabled = 0;
	config.repeatEnabled = 0;
	config.debounceHold = 150;
	ButtonsInit(&endstops,  (uint16_t *)&isrTime, &config);								// Init the button module with the timer it points to, and the config you've set up
	
	ButtonsSetButton(&endstops, &LIM_F_PIN, LIM_F_BIT, 1, LIMIT_FORWARD);				// Set user button index 0, to PIND.2, active low
	ButtonsSetButton(&endstops, &LIM_R_PIN, LIM_R_BIT, 1, LIMIT_REVERSE);				// Set user button index 1, to PIND.3, active low
	ButtonsEnableButton(&endstops, LIMIT_FORWARD);										// Enable button index 0
	ButtonsEnableButton(&endstops, LIMIT_REVERSE);										// Enable button index 1
	
	ButtonsRegisterCallback(&endstops, Release_Handler, BUTTONS_CALLBACK_RELEASED);		// Register a callback for the released callback
	ButtonsEnableCallback(&endstops, BUTTONS_CALLBACK_RELEASED);						// Enable the callbacks
	
	ButtonsAddModule(&endstops);														// Add the module to the task handler
}

void MotorStop(void) {
	// Disable motors!
	F_PWM = 0;
	R_PWM = 0;
	disableMotors = currentTime + 100;
}

int main(void)
{
	SetupPorts();
	SetupADC();	
	SetupTimers();
	SetupButtons();
	uart_init(UART_BAUD_SELECT_DOUBLE_SPEED(115200,F_CPU));
	
	if (!DataNvMemLoad()) {
		DataNvMemDefaults();
	}
	
	// init global variables	
	tuneMin = 0x02FF;
	tuneMax = 0;
	motorState = INIT;
	
	uart_puts("Ned was here!\r\n");
	
	sei();
	
	// crudely get the ADC going and get the average up to scratch	
	for (tempByte = 0; tempByte < 100; tempByte++) {
		speedPot.raw = ReadADC(ADC_SPEED);
		AverageADC(&speedPot, 3);
		_delay_ms(10);
	}
			
    /* Replace with your application code */
    while (1) 
    {
		cli();	// Atomic Block
		currentTime = isrTime;
		sei();
		
		switch (motorState) {
			case INIT:
				MotorStop();				
				endstops.held = 0;
				motorState = TUNE;
				break;
			
			// wait for the throttle to be brought all the way to one end stop	
			case TUNE:
				// if forward end stop, wait for reverse endstop/tune
				if (endstops.held & (1 << LIMIT_FORWARD)) {
					motorState = R_TUNE;
					nvMem.potMax = speedPot.avg - 2;	// calibrate max. In reality this will force the release to update the values
					dataChanged = 1;
					endstops.held &= ~(1 << LIMIT_FORWARD);
				}
				
				// if reverse end stop, wait for forward endstop/tune
				if (endstops.held & (1 << LIMIT_REVERSE)) {
					motorState = F_TUNE;
					nvMem.potMin = speedPot.avg + 2;	// calibrate min. In reality this will force the release to update the values
					endstops.held &= ~(1 << LIMIT_REVERSE);
				}
				break;
			
			// wait for the second end stop to be hit, before waiting to go to neutral	
			case F_TUNE:
				if (endstops.held & (1 << LIMIT_FORWARD)) {
					motorState = IDLE_WAIT;
					nvMem.potMax = speedPot.avg - 2;	// calibrate max. In reality this will force the release to update the values
					endstops.held = 0;
				}
				break;
			
			// wait for the second end stop to be hit, before waiting to go to neutral
			case R_TUNE:
				if (endstops.held & (1 << LIMIT_REVERSE)) {
					motorState = IDLE_WAIT;
					nvMem.potMin = speedPot.avg + 2;	// calibrate min. In reality this will force the release to update the values
					endstops.held = 0;
				}
				break;
			
			// wait for the throttle to be moved to/past neutral
			case IDLE_WAIT:
				if (DIFF(speedPot.avg, nvMem.potMid) < (DEAD_BAND << 1)) {
					motorState = IDLE;
				}
				break;
				
			case IDLE:
				// idle down motors if they are still running
				if (F_PWM || R_PWM) {
					if (TimeYet(rampSpeed)) {
						rampSpeed = currentTime + RAMP_DOWN;
						if (F_PWM)	F_PWM--;
						if (R_PWM)	R_PWM--;
					}
					
					//rampSpeed = currentTime + RAMP_DOWN;
					disableMotors = currentTime + 100;
				}
				else {
					// check what direction we want to go in, and sanity check for open pot
					if ((speedPot.avg >= 23) && (speedPot.avg < (nvMem.potMid - DEAD_BAND))) {
						motorState = R_DELAY;
						directionDelay = currentTime + 100;
					}
				
					// check what direction we want to go in, and sanity check for open pot
					if ((speedPot.avg <= 1000) && (speedPot.avg > (nvMem.potMid + DEAD_BAND))) {
						motorState = F_DELAY;
						directionDelay = currentTime + 100;
					}
					
					// after 30 seconds, lock out controls
					if (TimeYet(idleEnter + 30000)) {
						motorState = INIT;
					}
				}
				break;
			
			case F_DELAY:
				if (TimeYet(directionDelay)) {
					motorState = FORWARD;
					disableMotors = currentTime + 100;
					rampSpeed = currentTime;
					R_ENABLE = ENABLED;
					F_ENABLE = ENABLED;
				}
				break;
			
			case R_DELAY:
				if (TimeYet(directionDelay)) {
					motorState = REVERSE;
					disableMotors = currentTime + 100;
					rampSpeed = currentTime;
					R_ENABLE = ENABLED;
					F_ENABLE = ENABLED;
				}
				break;
			
			case FORWARD:
				disableMotors = currentTime + 100;
				curSpeed = F_PWM;
				
				// calc speed
				reqSpeed = SCALE(speedPot.avg, nvMem.potMid + DEAD_BAND, nvMem.potMax, MIN_SPEED, MAX_SPEED);
				if (LIM_FORWARD && reqSpeed > (MAX_SPEED - 10)) reqSpeed = MAX_SPEED;
				
				// set min speed, unless speed is 0.
				if (reqSpeed <= MIN_SPEED)
					reqSpeed = 0;
				
				// if nothing is happening, go to idle
				if ((curSpeed == 0) && (reqSpeed == 0)) {
					motorState = IDLE;
				}
				
				if (TimeYet(rampSpeed)) {
					rampSpeed = currentTime + RAMP_UP;	// slow delay when speeding up the motor
					
					if (reqSpeed > curSpeed) {
						F_PWM = ++curSpeed;
					}
					else if (reqSpeed < curSpeed) {
						rampSpeed = currentTime + RAMP_DOWN;	// we can slow down faster than speed up
						F_PWM = --curSpeed;
					}
				}
				break;
				
			case REVERSE:
				disableMotors = currentTime + 100;
				curSpeed = R_PWM;
				
				// calc speed
				reqSpeed = SCALE((nvMem.potMid - speedPot.avg) + nvMem.potMin, nvMem.potMin + DEAD_BAND, nvMem.potMid, MIN_SPEED, MAX_SPEED);
				if (LIM_REVERSE && reqSpeed > (MAX_SPEED - 10)) reqSpeed = MAX_SPEED;
				
				// set min speed, unless speed is 0.
				if (reqSpeed <= MIN_SPEED)
					reqSpeed = 0;
				
				// if nothing is happening, go to idle
				if ((curSpeed == 0) && (reqSpeed == 0))
				if ((curSpeed == 0) && (reqSpeed == 0)) {
					motorState = IDLE;
				}	
				
				if (TimeYet(rampSpeed)) {
					rampSpeed = currentTime + RAMP_UP;	// slow delay when speeding up the motor
					
					if (reqSpeed > curSpeed) {
						R_PWM = ++curSpeed;
					}
					else if (reqSpeed < curSpeed) {
						rampSpeed = currentTime + RAMP_DOWN;	// we can slow down faster than speed up
						R_PWM = --curSpeed;
					}
				}
				break;
			
			default:
				motorState = TUNE;
				break;
		}
		
		if (motorState != IDLE)	idleEnter = currentTime;
		
		if (TimeYet(disableMotors)) {
			disableMotors = currentTime + 50;
			F_ENABLE = DISABLED;
			R_ENABLE = DISABLED;
		}
		
		ButtonTaskHandler();
		
		// read the ADC every so often
		if (TimeYet(updateADC)) {
			updateADC = currentTime + 10;	// every 10mS
			
			speedPot.raw = ReadADC(ADC_SPEED);
			AverageADC(&speedPot, 3);				// average the readings just to get a nice smooth number
		}
		
		// just some debug ADC measurements
		if (TimeYet(sendSerial)) {
			//sendSerial = currentTime + 50;
			sprintf(tempString, "%3u, %3u, %3u, %3u - ", speedPot.avg, nvMem.potMin, nvMem.potMid, nvMem.potMax);
			uart_puts(tempString);
			
			sendSerial = currentTime+5;
			sprintf(tempString, "%2u, %3u, %3u, %u, %u\r\n", motorState, F_PWM, R_PWM, F_ENABLE, R_ENABLE);
			uart_puts(tempString);
		}
				
		// if the forward end stop is met, check what the largest ADC number is when its pressed
		if (LIM_FORWARD) {
			if (speedPot.avg > tuneMax) tuneMax = speedPot.avg;
		}
		
		// if the reverse end stop is met, check what the smallest ADC number is when its pressed
		if (LIM_REVERSE) {
			if (speedPot.avg < tuneMin) tuneMin = speedPot.avg;
		}
						
		// if we've been in idle for 1 sec, save data
		if (TimeYet(idleEnter + 1000)) {	
			// if the data has changed, update the memory
			if (dataChanged) {
				DataNvMemSave();
				uart_puts("\r\n\r\nSaving Memory!\r\n\r\n");
				dataChanged = 0;
			}
		}
	}
}

/***** Time Yet
 * Check if a specific time has happened yet or not
 ----------
 * @param - compTime, The time you want to compare to the current time to see if that has passed yet
 * @return - boolean, gives a true/false response
 *****/
uint8_t TimeYet(uint16_t compTime) {
 	if ((compTime - currentTime - 1) > 0xF000) {	// if the time has passed (will roll around when not serviced for 4095 counts)
    	return 1;									// the time has passed
    }
	else return 0;									// or else it has not yet passed
}

ISR(TIMER2_COMPA_vect) {
	isrTime++;
}
