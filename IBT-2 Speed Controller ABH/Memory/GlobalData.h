/*
 * GlobalData.h
 *
 * Created: 19/07/2016 6:32:06 p.m.
 *  Author: Ned
 */ 


#ifndef GLOBALDATA_H_
#define GLOBALDATA_H_

struct SavedData {
	// potData
	uint16_t potMax, potMin, potMid;
	
	uint8_t checksum;
};

extern struct SavedData nvMem;

void DataNvMemDefaults(void);
uint8_t DataNvMemLoad(void);
void DataNvMemSave(void);

#endif /* GLOBALDATA_H_ */