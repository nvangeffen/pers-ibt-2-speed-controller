/*
 * GlobalData.c
 *
 * Created: 19/07/2016 6:31:49 p.m.
 *  Author: Ned
 */ 

#include <string.h>
#include <avr/eeprom.h>
#include "GlobalData.h"

struct SavedData nvMem;

static uint8_t CalcChecksum(uint8_t *data, uint16_t dataSize) {
	uint8_t checksum = 0;
	
	while (--dataSize)
		checksum ^= *data++;
		
	return checksum;
}

void DataNvMemDefaults(void) {
	// LED
	nvMem.potMax = 612;
	nvMem.potMin = 412;
	nvMem.potMid = 512;
		
	nvMem.checksum = CalcChecksum((uint8_t*)&nvMem, sizeof(nvMem));
}

void DataNvMemSave(void) {
	struct SavedData tempData;
	uint8_t retries = 5;
	
	nvMem.checksum = CalcChecksum((uint8_t*)&nvMem, sizeof(nvMem));
	
	do {
		eeprom_busy_wait();
		eeprom_update_block(&nvMem, (void*)0x00, sizeof(nvMem));
		eeprom_busy_wait();
		eeprom_read_block(&tempData, (void*)0x100, sizeof(struct SavedData));
	} while (--retries && (memcmp(&nvMem, &tempData, sizeof(struct SavedData))));
}

uint8_t DataNvMemLoad(void) {
	struct SavedData tempData;
	uint8_t retries = 5;
	
	do {
		eeprom_busy_wait();
		eeprom_read_block(&tempData, (void*)0x00, sizeof(struct SavedData));
	} while (--retries && (CalcChecksum((uint8_t*)&tempData, sizeof(struct SavedData)) != tempData.checksum));
	
	if (retries) {
		memcpy(&nvMem, &tempData, sizeof(tempData));
	}
	
	return retries;
}