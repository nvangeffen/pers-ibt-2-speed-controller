/*
 * Hardware.h
 *
 * Created: 24/06/2016 12:10:35 p.m.
 *  Author: Ned
 */ 

#include "..\HeaderFiles\includes.h"

#ifndef HARDWARE_H_
#define HARDWARE_H_

// Hardware Specific Defines
#define LED_DDR		REGISTER_BIT(DDRB, 5)
#define LED			REGISTER_BIT(PORTB, 5)

#define RX_DDR		REGISTER_BIT(DDRD, 0)
#define RX_PUP		REGISTER_BIT(PORTD, 0)
#define TX_DDR		REGISTER_BIT(DDRD, 1)
#define TX			REGISTER_BIT(PORTD, 1)

#define LIM_F_PIN	PIND
#define LIM_F_BIT	2
#define LIM_F_DDR	REGISTER_BIT(DDRD, 2)
#define LIM_F_PUP	REGISTER_BIT(PORTD, 2)
#define LIM_FORWARD	!REGISTER_BIT(PIND, 2)

#define LIM_R_PIN	PIND
#define LIM_R_BIT	3
#define LIM_R_DDR	REGISTER_BIT(DDRD, 3)
#define LIM_R_PUP	REGISTER_BIT(PORTD, 3)
#define LIM_REVERSE	!REGISTER_BIT(PIND, 3)

// PWMs
#define R_ERROR		REGISTER_BIT(PIND, 6)
#define F_ERROR		REGISTER_BIT(PINB, 4)
#define R_ENABLE	REGISTER_BIT(PORTB, 0)
#define F_ENABLE	REGISTER_BIT(PORTD, 7)
#define R_PWM		OCR1A
#define F_PWM		OCR1B

#define R_EN_DDR	REGISTER_BIT(DDRB, 0)
#define F_EN_DDR	REGISTER_BIT(DDRD, 7)
#define R_PWM_DDR	REGISTER_BIT(DDRB, 1)
#define F_PWM_DDR	REGISTER_BIT(DDRB, 2)

#define POT_PUP		REGISTER_BIT(PORTC, 1)

#define ADC_SPEED		1

// Button index
#define LIMIT_FORWARD	0
#define LIMIT_REVERSE	1

#endif /* HARDWARE_H_ */