/*
 * includes.h
 *
 * Created: 24/06/2016 12:11:36 p.m.
 *  Author: Ned
 */ 


#ifndef INCLUDES_H_
#define INCLUDES_H_

#include "..\HeaderFiles\Hardware.h"
#include "..\HeaderFiles\NedsStandardHeader.h"
#include "..\USART\uart.h"
#include "..\ADC\ADC.h"
#include "..\Memory\GlobalData.h"
#include "..\Buttons\Buttons.h"
#include "..\Buttons\ConfigButtons.h"

extern adc_t speedPot;

#endif /* INCLUDES_H_ */