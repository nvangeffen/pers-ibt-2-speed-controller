/*
 * Buttons.c
 *
 * Created: 30/06/2016 11:58:32 a.m.
 *  Author: Ned van Geffen - King of Kings
 */ 

#include "Buttons.h"
#include <util/atomic.h>
#include <string.h>

// Some simple MAX and MIN functions
#ifndef MAX
#define MAX(a,b) 	(((a)>(b))?(a):(b))
#endif

#ifndef MIN
#define MIN(a,b) 	(((a)<(b))?(a):(b))
#endif

struct buttonsModule *firstModule;		// Pointer to the first module in the module linked list

/***** ButtonsInit
 * Set all the user settings to a module.
 * And clear all other bits and bobs.
 ----------
 * @param - module, The module being set up
 * @param - time,	Pointer to a time variable. Preferably uint16_t incremented at 1KHz (1mS)
 * @param - config,	Config struct with all the user settable settings.
 *****/
void ButtonsInit(struct buttonsModule *const module, uint16_t *time, const struct buttonsConfig *const config) {
	memset(module, 0, sizeof(struct buttonsModule));		// Clear the whole module
	
	module->curTime = time;								// Set the time pointer
	
	module->debouncePress = config->debouncePress;		// Copy all the settings
	module->debounceHold  = config->debounceHold;
	module->rampSpeed     = config->rampSpeed;
	module->rampEnabled   = config->rampEnabled;
	module->repeatMax     = config->repeatMax;
	module->repeatMin     = config->repeatMin;
	module->repeatEnabled = config->repeatEnabled;
	
	module->next = 0;									// Not needed, it should be 0 already
}

/***** ButtonsAddModule
 * This adds the module to the linked list so the task handler knows to update it.
 ----------
 * @param - module, The module to add to the list
 *****/
void ButtonsAddModule(struct buttonsModule *const module) {
	struct buttonsModule *nextModule;	
	
	if (!firstModule) firstModule = module;		// Set this module to the first module if there isnt one
	else {
		nextModule = firstModule;				// Start looping with the firstModule
		
		while (nextModule->next != 0) {			// and loop until you get to the end of the linked list
			nextModule = nextModule->next;
		}
	
		nextModule->next = module;				// add this module to the end of the linked list.
	}
}

/***** ButtonsRegisterCallback
 * Set the callback function pointer
 ----------
 * @param - module,			The module to set the callback in
 * @param - callbackFunc,	Function pointer to the callback handler
 * @param - callbackType,	Type of callback it is we are setting
 *****/
void ButtonsRegisterCallback(struct buttonsModule *const module, buttonsCallback_t callbackFunc, enum buttonsCallback callbackType) {
	module->callback[callbackType] = callbackFunc;		// Set the callback funtion for this type
	module->registeredCallback |= (1 << callbackType);	// Set the bit field for this type to say its registered
}

/***** ButtonsUnregisterCallback
 * Clear the callback function pointer
 ----------
 * @param - module,			The module to clear the callback in
 * @param - callbackType,	Type of callback it is we are clearing
 *****/
void ButtonsUnregisterCallback(struct buttonsModule *const module, enum buttonsCallback callbackType) {
	module->callback[callbackType] = 0;					// Set the callback funtion for this type
	module->registeredCallback &= ~(1 << callbackType);	// Clear the bit field for this type to say its unregistered
}

/***** ButtonsEnableCallback
 * Enable a callback function
 ----------
 * @param - module,			The module to enable the callback in
 * @param - callbackType,	Type of callback it is we are enabling
 *****/
void ButtonsEnableCallback(struct buttonsModule *const module, enum buttonsCallback callbackType) {
	// Set the bit field for this type to say its enabled
	module->enabledCallback |= (1 << callbackType);
}

/***** ButtonsDisableCallback
 * Disable a callback function
 ----------
 * @param - module,			The module to disable the callback in
 * @param - callbackType,	Type of callback it is we are disabling
 *****/
void ButtonsDisableCallback(struct buttonsModule *const module, enum buttonsCallback callbackType) {
	// Clear the bit field for this type to say its disabled
	module->enabledCallback &= ~(1 << callbackType);
}

/***** ButtonsSetButton
 * Set what the button is in the specific index of a module
 ----------
 * @param - module,		The module to enable the button in
 * @param - pinReg,		The register pointer of where the button is
 * @param - bitNum,		The index in the register where the button is we want.
 * @param - activeLow,	Whether the button is active low or not. (1 = active low, 0 = active high)
 * @param - buttonNum,	Button index we are setting
 *****/
void ButtonsSetButton(struct buttonsModule *const module, volatile uint8_t *pinReg, uint8_t bitNum, uint8_t activeLow, uint8_t buttonNum) {
	module->pins[buttonNum].pinReg = pinReg;						// Set the pin register
	module->pins[buttonNum].regMask = (1 << bitNum);				// Set the right mask for the index we want
	if (activeLow)												// If the button is actually active low
		 module->pins[buttonNum].activeLow = (1 << bitNum);		// Set the right bit mask to XOR the reult with later to invert the result
	else module->pins[buttonNum].activeLow = 0;					// If active high, just leave it as 0. No inverting required!
}

/***** ButtonsEnableButton
 * Enable a button in the module
 ----------
 * @param - module,		The module to enable the button in
 * @param - buttonNum,	Button index we are enabling
 *****/
void ButtonsEnableButton(struct buttonsModule *const module, uint8_t buttonNum) {
	// Set the bit field for this button to say its enabled
	module->enabledButtons |= (1 << buttonNum);
}

/***** ButtonsDisableButton
 * Disable a button in the module
 ----------
 * @param - module,		The module to disable the button in
 * @param - buttonNum,	Button index we are disabling
 *****/
void ButtonsDisableButton(struct buttonsModule *const module, uint8_t buttonNum) {
	// Clear the bit field for this button to say its disabled
	module->enabledButtons &= ~(1 << buttonNum);
}

/***** TimeDiff
 * Calculate the difference between 2 time points
 ----------
 * @param - past,	Past time
 * @param - future,	future time
 * @return Time difference between past and future time.
 *****/
static uint16_t TimeDiff(uint16_t past, uint16_t future) {
	if (((future - 1) - past) > 0xF000) return 0;
	else return future - past;
}

/***** ButtonTaskHandler
 * The main task that reads all the buttons for all the modules and
 * figures out if there are any button presses or not.
 ----------
 *****/
void ButtonTaskHandler(void) {
	uint16_t currentTime;
	uint16_t callbackMask;
	struct buttonsModule *module;
	uint32_t buttons;
	uint8_t buttonCount;
	uint8_t loop;
	
	// Loop through all the modules. First module location is in first module ptr
	module = firstModule;
	
	// As long as modules exist
	while (module) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { currentTime = *module->curTime;	}	// Atomic read of time
		
		for (loop = buttons = 0; loop < MAX_BUTTONS_PER_MODULE; loop++) {										// Loop through the buttons for this module.
			if ((*module->pins[loop].pinReg & module->pins[loop].regMask) ^ module->pins[loop].activeLow)		// Read the register and mask it with the mask. XOR them with any active low pins to invert the status
					buttons |=  (1 << loop);																		// Set the pin high if its active
			else buttons &= ~(1 << loop);																		// Clear it if its low
		}
			
		buttons &= module->enabledButtons;						// Mask out any disabled buttons
			
		#if MAX_BUTTONS_PER_MODULE <= 16
		buttonCount = __builtin_popcount(buttons);				// Count the number of high bits
		#elif MAX_BUTTONS_PER_MODULE <= 32
		buttonCount = __builtin_popcountl(buttons);
		#elif MAX_BUTTONS_PER_MODULE <= 64
		buttonCount = __builtin_popcountll(buttons);
		#else
		#error "Really? more than 64 buttons? are you crazy!?"
		#endif
		
		if (TimeDiff(module->time, currentTime) >= module->debounceHold) {					// If a button has been held
			if (module->decreaseFlag) {														// if the button count was lowered earlier but they have remained the same for the length of a hold time
				module->decreaseFlag = 0	;													// clear the flag that states it was lowered
				//module->released = module->lastStatus ^ buttons;							// Set which button of the set was released
				module->released = module->lastStatus;										// Set which button combo was released, even if some of them are still pressed!
				module->lastStatus = buttons;												// and set the button status to the currently pressed buttons
			}
			
			if (module->repeatEnabled)														// If repeat holds are enabled
				module->held = module->lastStatus;											// Set what buttons were held
			else if (!module->heldFlag) {													// but if no repeats are enabled,
				module->held = module->lastStatus;											// Set what buttons were held, only once though
			}
			module->time += module->repeat;													// set the time to repeat the next press
			if (module->rampEnabled) {														// If ramps are enabled, lower the repeat value to increase the button held repeat rate
				module->repeat = MAX(module->repeatMin, module->repeat - MAX(1,(module->repeat >> module->rampSpeed)));		// Set it to repeat - repeat >> rampSpeed. Make sure the end result is >= repeatMin, and repeat >> rampSpeed is at least 1!
			}
			module->heldFlag = 1;															// set the flag that states a button was held to stop repeat presses
		}

		if (!buttons) {																		// If no buttons are pressed
			if (module->heldFlag) {															// and the buttons were previously held
				module->released = module->lastStatus;										// Set what buttons were released
				module->heldFlag = 0;														// Clear the flag
			}
			else if (TimeDiff(module->time, currentTime) >= module->debouncePress) {			// but if the buttons werent held, but pressed for long enough to pass as a debounce
				module->pressed = module->lastStatus;										// Set what buttons were pressed
			}

			module->lastCount = 0;															// Clear the last count
			module->lastStatus = 0;															// Clear the last Status
			module->time = currentTime;														// clear the last press time
		}
		else if (buttonCount > module->lastCount) {											// if the number of buttons pressed has gone up
			module->time = currentTime;														// reset the time of last button presses.
			module->repeat = module->repeatMax;												// and reset the time between held repeats.
			
			module->lastCount = buttonCount;													// save it for next time.
			module->lastStatus = buttons;													// and set what buttons were pressed.
			module->decreaseFlag = 0;														// clear the flag that says the button presses just decreased.
		}
		else if (buttonCount && (buttonCount < module->lastCount)) {							// Or if the button count deceased but a button is still pressed
			module->time = currentTime;														// reset the time of last button presses.
			module->repeat = module->repeatMax;												// and reset the time between held repeats.
			
			module->lastCount = buttonCount;													// save the count for next time
			module->decreaseFlag = 1;														// set the flag to say this happened
		}
		else if (!module->decreaseFlag && (buttons != module->lastStatus)) {					// If someone changed button presses but not the count
			module->lastCount = 0;															// Force the count to change next time around so the code to set times etc isnt in 2 places.
		}
			
		module->lastTime = currentTime;														// Store the time so we know when we were here last
			
		callbackMask = module->enabledCallback & module->registeredCallback;					// Get all of the registered and enabled callbacks
			
		if ((callbackMask & (1 << BUTTONS_CALLBACK_PRESSED)) && (module->pressed)) {			// if a button is pressed, and the pressed callback is enabled
			(module->callback[BUTTONS_CALLBACK_PRESSED]) (module, module->pressed);			// Run the callback!
			module->pressed = 0;																// Clear the reason for the callback.
		}
			
		if ((callbackMask & (1 << BUTTONS_CALLBACK_HELD)) && (module->held)) {				// if a button is held, and the held callback is enabled
			(module->callback[BUTTONS_CALLBACK_HELD]) (module, module->held);				// Run the callback!
			module->held = 0;																// Clear the reason for the callback.
		}
			
		if ((callbackMask & (1 << BUTTONS_CALLBACK_RELEASED)) && (module->released)) {		// if a button is released, and the released callback is enabled
			(module->callback[BUTTONS_CALLBACK_RELEASED]) (module, module->released);		// Run the callback!
			module->released = 0;															// Clear the reason for the callback.
		}
			
		module = module->next;																// Off to the next module!
	}
}
